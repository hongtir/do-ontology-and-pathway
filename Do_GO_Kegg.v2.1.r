# Do_GO_Kegg.v2.1.r
# first virsion 2020.12.08
# final virsion 2021.08.27
# to remember my kid
# HDChen in CMUH



library("optparse")



## 描述参数的解析方式

nowdir=getwd()


option_list <- list(
  make_option(c("-g", "--GENE"),type = "character", default = NULL, action = "store", 
              help = "Gene Symbol list"  ),
  make_option(c("-m", "--MARK"), type = "character", default = 'test', action = "store", 
              help = "file prefix mark"),
  make_option(c("-p", "--Pvalue"), type = "double", default = 0.05, action = "store", 
              help = "P value filter, default is 0.05"))


## 解析參數

opt = parse_args(OptionParser(option_list = option_list, usage = "This Script is a test for arguments!"))

print(opt)
    
mark=opt$MARK
gene=opt$GENE
pvalue=opt$Pvalue

##  load package 


library("gplots")
library("clusterProfiler")
library("DOSE")
library("org.Hs.eg.db")
library("topGO")
library("pathview")  
library("edgeR")
library("GO.db")


    
## Read gene list

# check date is provided



    print('input file:')
    print(gene)

    df=read.table(gene, row.names = 1)
    rn=rownames(df)
    print(rn)




## ENTREZID
print('Transform to ENTREZID ID ...')

rnid <- bitr(rn, fromType = "SYMBOL", toType = "ENTREZID", OrgDb = org.Hs.eg.db)
    
id=rnid$ENTREZID
    
    
## GO term
print('Do GO term ... ')


go_model <- enrichGO(rnid$SYMBOL, OrgDb = org.Hs.eg.db, ont='ALL',pAdjustMethod = 'BH',pvalueCutoff = pvalue, qvalueCutoff = 0.2, keyType='SYMBOL')

gtname=paste(mark,'_GO_enrich.csv',sep ='')
write.csv((as.data.frame(go_model)),gtname,row.names =F)
    
    
### GO barplot
    
gbname=paste(mark,'_GO_bar.pdf',sep ='')
pdf(gbname, width = 12, height = 6)
barplot(go_model,showCategory=20,drop=T)
dev.off()
    
    
### dotplot
gdname=paste(mark,'_GO_dot.pdf',sep ='')
pdf(gdname, width = 12, height = 6)
dotplot(go_model,showCategory=20)
dev.off()
    
## GO: CC, BP, MF
    
go_CC <- enrichGO(id, OrgDb = org.Hs.eg.db, ont='CC',readable = TRUE,pAdjustMethod = 'BH',pvalueCutoff = pvalue,qvalueCutoff = 0.5,keyType = 'ENTREZID')
go_BP <- enrichGO(id, OrgDb = org.Hs.eg.db, ont='BP',readable = TRUE,pAdjustMethod = 'BH',pvalueCutoff = pvalue,qvalueCutoff = 0.5,keyType = 'ENTREZID')
go_MF <- enrichGO(id, OrgDb = org.Hs.eg.db, ont='MF',readable = TRUE,pAdjustMethod = 'BH',pvalueCutoff = pvalue,qvalueCutoff = 0.5,keyType = 'ENTREZID')
    
gbcname=paste(mark,'_GO_bar_CC.pdf',sep ='')
gbbname=paste(mark,'_GO_bar_BP.pdf',sep ='')
gbmname=paste(mark,'_GO_bar_MF.pdf',sep ='')
gdcname=paste(mark,'_GO_dot_CC.pdf',sep ='')
gdbname=paste(mark,'_GO_dot_BP.pdf',sep ='')
gdmname=paste(mark,'_GO_dot_MF.pdf',sep ='')

### GO group  barplot
    
    
pdf(gbcname, width = 12, height = 6)
barplot(go_CC,showCategory=20,drop=T)
dev.off()

pdf(gbbname, width = 12, height = 6)
barplot(go_BP,showCategory=20,drop=T)
dev.off()

pdf(gbmname, width = 12, height = 6)
barplot(go_MF,showCategory=20,drop=T)
dev.off()

### GO group dotplot

pdf(gdcname, width = 12, height = 6)
dotplot(go_CC,showCategory=20)
dev.off()

pdf(gdbname, width = 12, height = 6)
dotplot(go_BP,showCategory=20)
dev.off()

pdf(gdmname, width = 12, height = 6)
dotplot(go_MF,showCategory=20)
dev.off()

### GO group goplot

gpcname=paste(mark,'_GO_plot_CC.pdf',sep ='')
gpbname=paste(mark,'_GO_plot_BP.pdf',sep ='')
gpmname=paste(mark,'_GO_plot_MF.pdf',sep ='')

pdf(gpcname, width = 12, height = 12)
goplot(go_CC)
dev.off()

pdf(gpbname, width = 12, height =12)
goplot(go_BP,showCategory = 8)
dev.off()

pdf(gpmname, width = 12, height = 12)
goplot(go_MF)
dev.off()
    
    
    
## DO

xx <- enrichDO(gene = id,
              ont = "DO",
              pvalueCutoff = pvalue,
              pAdjustMethod = "BH",
              minGSSize = 1,
              maxGSSize = 500,
              qvalueCutoff = 0.05,
              readable = TRUE)

dt=as.data.frame(xx)

dtname=paste(mark,'_DO_enrich.csv',sep='')
write.csv(dt,dtname,row.names =FALSE)


### DO barplot
dbname=paste(mark,'_DO_bar.pdf',sep='')
pdf(dbname, width = 12, height = 6)
barplot(xx,showCategory=20,drop=T)
dev.off()

###DO dotplot
ddname=paste(mark,'_DO_dot.pdf',sep='')
pdf(ddname, width = 12, height = 6)
dotplot(xx,showCategory=20)
dev.off()


    
    
    
## KEGG
    
    

print('Do KEGG')


kk <- enrichKEGG(gene = id, organism = 'hsa', pvalueCutoff = 1)


kk2<- setReadable(kk, org.Hs.eg.db,"ENTREZID")


rnkk=as.data.frame(kk2)


### save kegg data

ktname=paste(mark,'_KEGG_enrich.csv',sep='')
write.csv(rnkk,ktname,row.names =FALSE)


### kegg barplot
kbname=paste(mark,'_KEGG_bar.pdf',sep='')
pdf(kbname, width = 12, height = 6)
barplot(kk,showCategory=20,drop=T)
dev.off()

### kegg dotplot
kdname=paste(mark,'_KEGG_dot.pdf',sep='')
pdf(kdname, width = 12, height = 6)
dotplot(kk,showCategory=20)
dev.off()


### kegg Pathway

mydir=getwd()
setwd(mydir)
kpname=paste(mark,'_KEGG_pathway',sep='')
dir.create(kpname)
setwd(kpname)


kkid=rnkk[which(rnkk$p.adjust<=pvalue),"ID"]


for ( x in kkid ) { pathview(gene.data=df, pathway.id=x, species="hsa",gene.idtype ="SYMBOL",limit = list(gene=c(-5,5)) ,out.suffix =mark, same.layer = F)}

setwd(mydir)
