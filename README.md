# Do Ontology and Pathway

**Do_GO_Kegg.v2.1.r**

Do GO, DO and KEGG Pathway  

## Getting started

### Installation Environment 

R script is necessary, for ease of installation, 

suggest use the docker file:`docker pull jupyter/datascience-notebook ` \
or  package free docker file: `docker pull hongtir/cmuh_jnrp`

and run script in the container or jupyter notebook.


### install package

```
Rscript R_packages_install.r
```

### quick start

```
Rscript Do_GO_Kegg.r  -g test_GeneSymbol_list.txt -m Mytest
```

### Options:

```
	-g GENE, --GENE=GENE
		Gene Symbol list

	-m MARK, --MARK=MARK
		file prefix mark

	-p PVALUE, --Pvalue=PVALUE
		P value filter, default is 0.05

	-h, --help
		Show this help message and exit

```

### Gene Symbol list format:

```
A2M     1.10701964627868
A4GALT  -2.59559777112453
AADAC   2.68216009281556
AADACP1 1.15590359652703
```



