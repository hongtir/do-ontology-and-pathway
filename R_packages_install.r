# first version: 2020.12.08
# final version: 2021.08.27
# HDChen in CMUH



# set a CRAN mirror
#Automatic redirection to servers worldwide, currently sponsored by Rstudio

local({r = getOption("repos")
r["CRAN"] = "https://cloud.r-project.org/"
options(repos=r)})

# install Packages

if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install(c('gplots','clusterProfiler','DOSE','topGO','pathview','edgeR','org.Hs.eg.db','corrplot','optparse'))




